module gitlab.com/NebulousLabs/Sia-Ant-Farm

go 1.15

require (
	github.com/julienschmidt/httprouter v1.3.0
	gitlab.com/NebulousLabs/Sia v1.5.5
	gitlab.com/NebulousLabs/encoding v0.0.0-20200604091946-456c3dc907fe
	gitlab.com/NebulousLabs/errors v0.0.0-20200929122200-06c536cf6975
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20181011194642-3a71999ed0d3
	gitlab.com/NebulousLabs/log v0.0.0-20201012072136-659df99ce4a1
	gitlab.com/NebulousLabs/merkletree v0.0.0-20200118113624-07fbf710afc4
	gitlab.com/NebulousLabs/threadgroup v0.0.0-20200608151952-38921fbef213
)
